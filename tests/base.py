"""
    Appellation: base
    Contributors: FL03 <jo3mccain@icloud.com> (https://gitlab.com/FL03)
    Description:
        ... Summary ...
"""
import candyland
from candyland.actors.manage.fs import Filer, try_extend_path


def test_package_version():
    assert candyland.__version__ == "0.1.3"


def test_file_manager():
    f = Filer()
    print(f.walk("Desktop")[0].root)
    assert f.walk("Desktop")[0].root == try_extend_path(path="Desktop")
