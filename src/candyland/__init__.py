__name__ = "candyland"
__package__ = "candyland"
__version__ = "0.1.5"

__import__("candyland.actors", globals(), locals())
__import__("candyland.components", globals(), locals())
__import__("candyland.core", globals(), locals())
__import__("candyland.data", globals(), locals())
