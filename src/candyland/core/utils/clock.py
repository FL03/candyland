"""
    Appellation: clock
    Contributors: FL03 <jo3mccain@icloud.com> (https://gitlab.com/FL03)
    Description:
        ... Summary ...
"""
from datetime import datetime


def timestamp() -> str:
    return str(datetime.now())
