"""
    Appellation: structs
    Contributors: FL03 <jo3mccain@icloud.com> (https://gitlab.com/FL03)
    Description:
        ... Summary ...
"""


def list_from(a, *args):
    return [a, *args]


def merge_lists(a: list, b: list) -> list:
    return [*a, *b]
